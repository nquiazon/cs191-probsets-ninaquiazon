package piwords;

import static org.junit.Assert.*;

import org.junit.Test;

public class BaseTranslatorTest {
    @Test
    public void basicBaseTranslatorTest() {
        // Expect that .01 in base-2 is .25 in base-10
        // (0 * 1/2^1 + 1 * 1/2^2 = .25)
        int[] input = {0, 1};
        int[] expectedOutput = {2, 5};
        assertArrayEquals(expectedOutput,
                          BaseTranslator.convertBase(input, 2, 10, 2));
        
        int input1[] = {5, 7};
        int expectedOutput1[] = {7, 3};
        assertArrayEquals(expectedOutput1,
                BaseTranslator.convertBase(input1, 8, 10, 2));
        
        int input2[] = {8,6,2,3,6};
        int expectedOutput2[] = {5,2,3,9,7};
        assertArrayEquals(expectedOutput2,
                BaseTranslator.convertBase(input2, 16, 10, 5));
        
        int input3[] = {8,7,0,0};
        int expectedOutput3[] = {5,2,7,3};
        assertArrayEquals(expectedOutput3,
                BaseTranslator.convertBase(input3, 16, 10, 4));
        
  
        int[] a={-1,7};
        int[] b={0,1,0,2};
        assertArrayEquals(null,
                BaseTranslator.convertBase(b, 2, 8, 2));
        assertArrayEquals(null,
                BaseTranslator.convertBase(a, 2, 8, 2));
       
        int[] b1 = {};
        assertArrayEquals(null,
                BaseTranslator.convertBase(b1, 1, 8, 2));
        assertArrayEquals(null,
                BaseTranslator.convertBase(b1, 2, 1, 2));
        assertArrayEquals(null,
                BaseTranslator.convertBase(b1, 2, 8, 0));
        
        
    }
}
