package piwords;

import static org.junit.Assert.*;

import org.junit.Test;

public class PiGeneratorTest {
    @Test
    public void basicPowerModTest() {
        // 5^7 mod 23 = 17
        assertEquals(17, PiGenerator.powerMod(5, 7, 23));
        assertEquals(6, PiGenerator.powerMod(6, 4, 15));
        assertEquals(8, PiGenerator.powerMod(8, 3, 12));
        
        int output1[] = {2, 4, 3, 15, 6, 10, 8, 8};
        assertArrayEquals(output1, PiGenerator.computePiInHex(8));
    }

}
