import java.util.Calendar;
import java.util.GregorianCalendar;

public class RulesOf6005 {

	public static boolean hasFeature(String name){
		if(name.equalsIgnoreCase("lectures")==true || name.equalsIgnoreCase("recitations")==true ||name.equalsIgnoreCase("text")==true ||name.equalsIgnoreCase("problem sets")==true||name.equalsIgnoreCase("code review")==true ||name.equalsIgnoreCase("returning")==true ||name.equalsIgnoreCase("projects")==true||name.equalsIgnoreCase("team meetings")==true ||name.equalsIgnoreCase("quizzes")==true){
			return true;
		}
		return false;
	}
	
	public static int computeGrade(int quiz, int pset, int project, int participation){
		double rawGrade =(quiz*0.2)+(pset*0.4)+(project*0.3)+(participation*0.1);
		int finalGrade = (int) rawGrade;
		double toUp = finalGrade + .5;
		if(rawGrade >= toUp){
			finalGrade = (int)(rawGrade+.5);
		}
		return finalGrade;
	}
	
	public static Calendar extendDeadline(int request, int budget, Calendar duedate){
		if(budget!=0){
			if(request>=budget){
				if(budget>=3)
					duedate.add(Calendar.DATE,3);
				else
					duedate.add(Calendar.DATE,budget);
			}else{
				if(budget>=3)
					duedate.add(Calendar.DATE,3);
				else
					duedate.add(Calendar.DATE, request);
			}
		}
		return duedate;
		
	}
	
	public static void main(String[] args){
		System.out.println("Has feature QUIZZES: " + hasFeature("QUIZZES"));
		System.out.println("My grade is: " + computeGrade(60, 40, 50, 37));
		Calendar duedate = new GregorianCalendar();
		duedate.set(2011, 8, 9, 23, 59, 59);
		System.out.println("Original due date: " + duedate.getTime());
		System.out.println("New due date:" + extendDeadline(4, 2, duedate).getTime());
	}
}